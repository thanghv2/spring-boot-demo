# Spring boot application demo
## features
 - Gradle file
 - Spring boot application config & profile
 - Spring repository data & junit test with h2
 - Spring Actuator  
 - Domain by feature package
 - Value Object with Lombok & MapStruct example (ParentVO)
 - Lombok Value & Data & Builder
 - Lombok with Json Jackson Mapper
## TODO
 - Add code gen for Swagger
 - Spring Validator
 - Swagger rest API viewer