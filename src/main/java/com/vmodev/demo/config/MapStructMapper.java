package com.vmodev.demo.config;

import com.vmodev.demo.domain1.ParentEntity;
import com.vmodev.demo.domain1.ParentVO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MapStructMapper {
    ParentVO parentEntityToDto(ParentEntity parentEntity);
}
