package com.vmodev.demo.domain1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class ParentServiceImpl implements ParentService {
    private final ParentRepository parentRepository;

    @Override
    public List<ParentVO> getAll() {
        return parentRepository.findAll().stream().map(ParentVO::fromEntity).collect(Collectors.toList());
    }
}
