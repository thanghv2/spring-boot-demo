package com.vmodev.demo.domain1;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParentRepository extends JpaRepository<ParentEntity, Long> {
    ParentEntity findByName(String name);
    ParentEntity findByStatus(ParentEntity.Status status);
}
