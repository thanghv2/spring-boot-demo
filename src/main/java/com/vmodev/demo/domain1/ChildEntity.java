package com.vmodev.demo.domain1;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class ChildEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDate birthDay;

    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = false)
    ParentEntity parent;

}
