package com.vmodev.demo.domain1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/parent")
@Slf4j
@AllArgsConstructor
public class ParentController {
    private final ParentServiceImpl parentService;

    @GetMapping
    List<ParentVO> getAll() {
        return parentService.getAll();
    }
}
