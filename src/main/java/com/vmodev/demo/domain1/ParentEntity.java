package com.vmodev.demo.domain1;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Data
public class ParentEntity {
    public enum Status {INITED, ACTIVE, DELETED}
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private LocalDateTime createdAt;
    private LocalDateTime modifyAt;

    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "parent")
    private Set<ChildEntity> children;

}
