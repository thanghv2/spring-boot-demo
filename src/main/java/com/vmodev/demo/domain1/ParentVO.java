package com.vmodev.demo.domain1;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Value
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParentVO {
    private Long id;
    private String name;
    private LocalDateTime createdAt;
    private LocalDateTime modifyAt;
}
