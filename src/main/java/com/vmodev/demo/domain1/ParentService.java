package com.vmodev.demo.domain1;

import java.util.List;

public interface ParentService {
    List<ParentVO> getAll();
}
