package com.vmodev.demo;

import com.vmodev.demo.config.MapStructMapper;
import com.vmodev.demo.domain1.ParentEntity;
import com.vmodev.demo.domain1.ParentVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class MapStructMapperTest {
    @Autowired
    private MapStructMapper mapStructMapper;

    @Test
    public void testParentMapper() {
        //GIVEN
        ParentEntity entity = new ParentEntity();
        entity.setId(1L);
        ParentVO compare = ParentVO.builder().id(1L).build();

        //WHEN
        ParentVO vo = mapStructMapper.parentEntityToDto(entity);
        //THEN
        assertEquals(compare, vo);
    }
}
