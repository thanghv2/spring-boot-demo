package com.vmodev.demo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
@Data
class DemoApplicationTests {
	private String test = """
		sss \s
			""";

	@Test
	void contextLoads() {
		this.getTest();
	    log.debug("context loaded %s".formatted(this.getTest()));

	}

}
