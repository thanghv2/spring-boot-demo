package com.vmodev.demo.domain1;

import com.vmodev.demo.DemoApplication;
import com.vmodev.demo.domain1.ParentEntity;
import com.vmodev.demo.domain1.ParentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = DemoApplication.class)
public class ParentRepositoryTest {
    @Autowired
    private ParentRepository parentRepository;

    @Test
    @Transactional
    public void given_data_then_found() {
        ParentEntity parent = new ParentEntity();
        parent.setName("sss");
        parent.setStatus(ParentEntity.Status.ACTIVE);
        parentRepository.save(parent);

        ParentEntity search = parentRepository.findByName("sss");
        ParentEntity search2 = parentRepository.findByName("ssas");
        ParentEntity search3 = parentRepository.findByStatus(ParentEntity.Status.ACTIVE);
        assertThat(search).isNotNull();
        assertThat(search3).isNotNull();
        assertThat(search2).isNull();
    }

    @Test
    @Transactional
    public void givenGenericEntityRepository_whenSaveAndRetreiveEntity_thenOK() {
        ParentEntity parent = new ParentEntity();
        parent.setCreatedAt(LocalDateTime.now());
        parent.setStatus(ParentEntity.Status.INITED);
        ParentEntity save = parentRepository.save(parent);

        ParentEntity foundEntity = parentRepository.getById(save.getId());

        assertNotNull(foundEntity);
        assertEquals(save.getCreatedAt(), foundEntity.getCreatedAt());
        assertEquals(save.getStatus(), foundEntity.getStatus());
    }
}
