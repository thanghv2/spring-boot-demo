package com.vmodev.demo.domain1;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParentVOTest {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testSerialize() throws IOException {
        ParentVO point = ParentVO.builder().id(1L).build();
        String json = objectMapper.writeValueAsString(point);
        assertEquals("""
                {"id":1,"name":null,"createdAt":null,"modifyAt":null}""", json);
    }

    @Test
    public void testDeserialize() throws IOException {
        String json = """
                {"id":10}""";
        ParentVO point = objectMapper.readValue(json, ParentVO.class);
        assertEquals(ParentVO.builder().id(10L).build(), point);
    }

    @Test
    public void testVO() {
        ParentVO vo1 = ParentVO.builder().id(1L).build();

        assertThat(vo1).isNotNull();
    }
}
